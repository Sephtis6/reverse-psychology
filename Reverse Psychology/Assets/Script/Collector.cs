﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{

    public AudioClip soundToPlay;
    public float score;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Pawn pawn = collision.gameObject.GetComponent<Pawn>();
        HumanController CC = pawn.CC;
        CC.gm.sfxAudio.PlayOneShot(soundToPlay, CC.gm.sfxAudio.volume);
        //CC.SetScore(score);
        Destroy(this.gameObject);
    }
}
