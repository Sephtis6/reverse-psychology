﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerCamera : MonoBehaviour
{

    //variables used
    public Vector3 offset;
    private Transform tf;

    public HumanController CC;
    public float zRotation;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        CC = GameObject.Find("CharacterController").GetComponent<HumanController>();
        CC.FPC = GetComponent<FollowPlayerCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        //position of camera changes to target objects current stransform + the offset
        //then has the camera look towards the objects position
        tf.position = CC.pawn.tf.position + offset;
        //tf.LookAt(CC.pawn.tf.position);
      
    }
}
