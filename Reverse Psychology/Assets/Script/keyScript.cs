﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keyScript : MonoBehaviour
{
    public GameObject respawnPoint;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Pawn pawn = collision.gameObject.GetComponent<Pawn>();
        HumanController CC = pawn.CC;
        CC.gateKey = true;
        CC.playerRespawnPoint = respawnPoint;
        CC.SetKeyStatus();
    }
}
