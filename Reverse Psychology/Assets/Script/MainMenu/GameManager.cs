﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //has the game manager be a static instance
    public static GameManager instance;
    //connects to the tankdata
    //[HideInInspector] public TankData tankData;
    //controls the input controller and keeps track of the player scores
    public float playerScore;
    public float playerLives;
    public float playerLivesStatic;
    //public float numEnemyTanks;
    [HideInInspector] public Transform tf;
    public AudioSource volumeAudio;
    public AudioSource sfxAudio;
    public List<AudioClip> audioClips;
    public AudioClip buttonPress;

    public Pawn playerPawn;


    [HideInInspector] public GameManager gm;

    // Use this for initialization
    void Awake () {
        //if there is more then one gamemanager it destroys and extras
		if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy (gameObject);
        }
	}
    void Start()
    {
        playerLivesStatic = playerLives;
        buttonPress = audioClips[0];
        gm = GetComponent<GameManager>();
        //points somewhere
        tf = GetComponent<Transform>();
    }
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //SceneManager.LoadScene(0);
            //if unity editor
            //UnityEditor.EditorApplication.isPlaying = false;
            //else
            Application.Quit();
        }

        //if tab is pressed
        if (Input.GetKeyDown(KeyCode.P))
        {
            //check whether the timescale is 1 or 0
            //if 0 set it to 1 and deactivate the canvas
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
            }
            //if 1 set it to 0 and activate the canvas
            else if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
        }

    }
}
