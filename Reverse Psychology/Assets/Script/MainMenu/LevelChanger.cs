﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    private GameManager gm;

    private void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void Update()
    {

    }

    //allows to set what level to go to on button press
    public void LoadByIndex(int sceneIndex)
    {
        gm.playerLives = gm.playerLivesStatic;
        SceneManager.LoadScene(sceneIndex);
    }



    //quit the game apllication
    public void Quit()
    {
        //if untiy editor
        //UnityEditor.EditorApplication.isPlaying = false;
        //else
        Application.Quit();
    }
}
