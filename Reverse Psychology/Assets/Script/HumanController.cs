﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HumanController : Controller {

    public Transform tf;
    public float speed;
    public FollowPlayerCamera FPC;
    public Camera mainCamera;

    public float jumpForce;
    public float jumpsLeft;
    public float jumpsAvailable;
    public float timeTillJumpRespawn;
    public float timeTillJumpRespawnStatic;
    public Text JumpsLeftText;

    public float lives = 3;
    public Text LifeText;

    public float score;
    public Text keyText;

    [HideInInspector] public GameManager gm;
    public GameObject playerPrefab;
    public GameObject playerRespawnPoint;
    public GameObject player;

    public bool gateKey = false;


    public bool isGrounded;
    public float groundDistance = 0.1f;

    public int loseLevelToLoad;

    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode rightKey = KeyCode.D;
    public KeyCode leftKey = KeyCode.A;
    public KeyCode upKey = KeyCode.W;
    public KeyCode downKey = KeyCode.S;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        tf = GetComponent<Transform>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        lives = gm.playerLives;
        setPawn();
        gm.volumeAudio.clip = gm.audioClips[1];
        gm.volumeAudio.loop = true;
        gm.volumeAudio.Play();
        jumpsLeft = jumpsAvailable;
        score = gm.playerScore;
        setLifeText();
        SetJumpsLeft();
        SetKeyStatus();
        isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {
        timeTillJumpRespawn -= Time.deltaTime;
        if (timeTillJumpRespawn <= 0)
        {
            if (jumpsLeft < jumpsAvailable)
            {
                jumpsLeft += 1;
            }
            else if (jumpsLeft == jumpsAvailable)
            {
                jumpsLeft = jumpsAvailable;
            }
            SetJumpsLeft();
            timeTillJumpRespawn = timeTillJumpRespawnStatic;
        }
        Vector2 movementVector = Vector2.zero;
        //move straight
        if (Input.GetKey(upKey))
        {
            movementVector.y = 1;
        }
        //rotate left
        if (Input.GetKey(leftKey))
        {
            movementVector.x = -1;
        }
        //rotate right
        if (Input.GetKey(rightKey))
        {
            movementVector.x = 1;
        }
        if (Input.GetKey(downKey))
        {
            movementVector.y = -1;
        }

        pawn.Move(movementVector);
        // Jump
        if (Input.GetKeyDown(jumpKey))
        {
            pawn.Jump();
        }

        if (lives <= 0)
        {
            SceneManager.LoadScene(loseLevelToLoad);
        }
    }

    public void Spawn()
    {
        player = Instantiate(playerPrefab, playerRespawnPoint.transform.position, playerRespawnPoint.transform.rotation);
    }

    public void setLifeText()
    {
        LifeText.text = "Lives Left: " + lives.ToString();
        gm.playerLives = lives;
    }

    public void InstantiatePlayer()
    {
        Instantiate(playerPrefab, playerRespawnPoint.transform.position, Quaternion.identity);
    }

    public void setPawn()
    {
        gm.playerPawn = pawn;
    }

    public void SetKeyStatus()
    {
        if (gateKey == false)
        {
            keyText.text = "Key Missing ";
        }
        if (gateKey == true)
        {
            keyText.text = "Key Found";
        }
    }

    public void SetJumpsLeft()
    {
        JumpsLeftText.text = "Jumps Left: " + jumpsLeft.ToString();
    }
}
