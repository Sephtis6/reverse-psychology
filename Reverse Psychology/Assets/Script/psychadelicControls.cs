﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class psychadelicControls : MonoBehaviour
{
    public bool switchCamera;
    public bool switchControls;
    public bool switchSpeedJump;
    public int randomNumber;
    public float timeTillReset = 15;
    public float timeTillResetStatic;

    // Use this for initialization
    void Start()
    {
        timeTillResetStatic = timeTillReset;
        timeTillReset = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        timeTillReset -= Time.deltaTime;
        if (timeTillReset <= 0)
        {
            if (collision.gameObject.tag == "Player")
            {
                Pawn pawn = collision.gameObject.GetComponent<Pawn>();
                HumanController CC = pawn.CC;
                if (switchControls = true)
                {
                    if (CC.jumpKey == KeyCode.Space)
                    {
                        CC.jumpKey = KeyCode.W;
                    }
                    else if (CC.jumpKey == KeyCode.W)
                    {
                        CC.jumpKey = KeyCode.Space;
                    }
                    if (CC.rightKey == KeyCode.D)
                    {
                        CC.rightKey = KeyCode.A;
                    }
                    else if (CC.rightKey == KeyCode.A)
                    {
                        CC.rightKey = KeyCode.D;
                    }
                    if (CC.leftKey == KeyCode.A)
                    {
                        CC.leftKey = KeyCode.D;
                    }
                    else if (CC.leftKey == KeyCode.D)
                    {
                        CC.leftKey = KeyCode.A;
                    }
                }
                if (switchCamera = true)
                {
                    CC.mainCamera.transform.Rotate(0, 0, 180);
                }
                if (switchSpeedJump = true)
                {
                    randomNumber = Random.Range(1, 10);
                    CC.speed = randomNumber;
                    randomNumber = Random.Range(1, 10);
                    CC.jumpForce = randomNumber;
                }
            }
            timeTillReset = timeTillResetStatic;
        }
    }
}
