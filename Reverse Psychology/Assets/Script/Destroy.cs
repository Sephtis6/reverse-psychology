﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Pawn pawn = collision.gameObject.GetComponent<Pawn>();
        HumanController CC = pawn.CC;
        CC.gm.sfxAudio.PlayOneShot(CC.gm.audioClips[0], CC.gm.sfxAudio.volume);
        Destroy(pawn.gameObject);
        CC.lives -= 1;
        CC.setLifeText();
        pawn.CC.InstantiatePlayer();
    }
}
