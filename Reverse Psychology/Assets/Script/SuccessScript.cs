﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SuccessScript : MonoBehaviour
{

    public int levelToLoad;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        Pawn pawn = collision.gameObject.GetComponent<Pawn>();
        HumanController CC = pawn.CC;
        if (CC.gateKey == true)
        {
            SceneManager.LoadScene(levelToLoad);
        }
    }
}
